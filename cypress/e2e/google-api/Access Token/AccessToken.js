import {AccessTokenConstants} from "./constants/AccessTokenConstants";
export class AccessToken{
    getAccessToken() {
        cy.log('Getting the access token')
        cy.request({
            method: 'POST',
            url: AccessTokenConstants.URL,
            body: {
                grant_type: 'refresh_token',
                client_id: AccessTokenConstants.CLIENT_ID,
                client_secret: AccessTokenConstants.CLIENT_SECRET,
                refresh_token:AccessTokenConstants.REFRESH_TOKEN,
            },
        }).then(({ body }) => {
            let {access_token, id_token} = body
            Cypress.env("access_token", access_token);
        })
    }
}