import {CredentialsConstants} from "./Constants/CredentialsConstants";
export class Credentials{
    getCredentials(role) {
        let index = -1;
        cy.request({
            method: 'GET',
            url: CredentialsConstants.URL,
            headers: { Authorization: `Bearer ${Cypress.env("access_token")}` },
        }).then(({ body }) => {
            for(let i =0; i < body["values"].length; i++) {
                if(body["values"][i][0] === role)
                    index = i;
            }
            if(index > 0){
                let email = body["values"][index][1]
                let password = body["values"][index][2]
                cy.wrap(email).as('email')
                cy.wrap(password).as('password')
            }
        })
    }
}