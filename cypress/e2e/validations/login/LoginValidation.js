import {LoginConstants} from "./constants/LoginConstants";

export class LoginValidation {
    visitLoginPage() {
        cy.visit(LoginConstants.LOGIN_URL)
    }

    enterEmail(email){
        cy.get(LoginConstants.EMAIL_INPUT).type(email)
    }
    enterPassword(password){
        cy.get(LoginConstants.PASSWORD_INPUT).type(password)
    }

    clickOnProceedButton() {
        cy.get(LoginConstants.LOGIN_BUTTON).click()
    }
}