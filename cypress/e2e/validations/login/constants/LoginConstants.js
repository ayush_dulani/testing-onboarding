export class LoginConstants {
    static LOGIN_URL = "/login"
    static EMAIL_INPUT = ".js-login-field"
    static PASSWORD_INPUT = "#password"
    static LOGIN_BUTTON = ".js-sign-in-button"
}