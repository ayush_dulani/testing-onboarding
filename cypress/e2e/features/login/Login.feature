Feature: Login Flow Test

  Scenario: Login with Super Admin
    Given I visited login page
    Then I logged in with "Super Admin"

  Scenario: Login with Admin
    Given I visited login page
    Then I logged in with "Admin"

  Scenario: Login with User
    Given I visited login page
    Then I logged in with "User"
