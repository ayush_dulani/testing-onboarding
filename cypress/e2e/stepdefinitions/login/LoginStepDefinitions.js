import {LoginTest} from "../../tests/login/LoginTest";

const loginTests = new LoginTest()

Given("I visited login page", () => {
    loginTests.visitLoginPage()
})

Then("I logged in with {string}", (user) => {
    loginTests.performLogin(user)
})