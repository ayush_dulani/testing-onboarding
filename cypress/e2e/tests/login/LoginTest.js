import {LoginValidation} from "../../validations/login/LoginValidation";
import {Credentials} from "../../google-api/Credentials/Credentials";

const credentials = new Credentials()

const login = new LoginValidation()
let access_token
export class LoginTest {
    visitLoginPage() {
        login.visitLoginPage()
    }

    performLogin(user){
        credentials.getCredentials(user);
        
        cy.get('@email').then((email) => {
            login.enterEmail(`${email}`)
        });
        cy.get('@password').then((password) => {
            login.enterPassword(`${password}`)
        });
        login.clickOnProceedButton()
    }
}